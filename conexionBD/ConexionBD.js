const {Client} =  require('pg')

const datosConexion = {
    "user" : "postgres",
    "host" : "localhost",
    "database" : "apidiscreta",
    "password" : "root",
    "port" : "5432"
}


const conexion = () => {return new Client(datosConexion)};




module.exports =  {conexion};