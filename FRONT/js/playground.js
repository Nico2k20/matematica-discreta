const radio = 10
const pi = 2 * Math.PI 
let arrVertices = [];

/*Elementos Aristas*/
const $template = document.getElementById("template-aristas").content;
const $seccion = document.querySelector(".aristas");
const $fragmento = document.createDocumentFragment();


const dibujarVertice =(e)=>{
    
    let ClientRect = canvas.getBoundingClientRect();
    let x = Math.round(e.clientX - ClientRect.left);
	let y = Math.round(e.clientY - ClientRect.top);
    let dibujarCanvas = e.target.getContext('2d');
    
    arrVertices.push(
        {
            "posX" : x,
            "posY" : y,
            "numeroVertice" : arrVertices.length
        }
    )

  // dibujarVecindad(e)
   dibujarAristas(arrVertices.length);
   // dibujarCanvas.fillStyle = "rgb(200,0,0)";
    //dibujarCanvas.fillRect(x, y, 10, 10)

    dibujarCanvas.beginPath();
    dibujarCanvas.arc(x, y, radio, 0, pi, false)
    dibujarCanvas.fillStyle = '#006DB2';
    dibujarCanvas.fill();
    dibujarCanvas.stroke();

    console.log(arrVertices)
}

const obtenerIndice=(e)=>{

    /*
        algoritmo que convierte el indice de una dimension a dos dimensiones  
    */
    limpiarAristasDelCanvas();
    let indice = 0;
    let indiceSuperior=0;


    $seccion.querySelectorAll(".vecindad-arista").forEach(element => {
       if(element.checked){
        dibujarVecindad(e,{"index1":indice,"index2" :indiceSuperior})
        }
       if(indice==(arrVertices.length-1)){
            indiceSuperior++
            indice=0
       }
       else
          indice++;
    });


    

}




const dibujarVecindad = (e,punto)=>{
    let dibujarCanvas = document.getElementById("canvas").getContext('2d');
    let v1 = arrVertices[punto["index1"]];
    let v2 = arrVertices[punto["index2"]];

    dibujarCanvas.lineWidth = 3;
    dibujarCanvas.strokeStyle = "black";
    dibujarCanvas.beginPath();
    dibujarCanvas.moveTo(v1.posX, v1.posY);
    dibujarCanvas.lineTo(v2.posX, v2.posY);
    dibujarCanvas.stroke();
}




const limpiarCanvas = ()=>{
    const canvas = document.getElementById("canvas") 
    let dibujar = canvas.getContext("2d");

    /*Limpiar canvas*/
    dibujar.clearRect(0,0,500,500)
    arrVertices = []

    /*Limpiar aristas*/
    dibujarAristas(0)

}


const dibujarAristas = (cantAristas)=>{
  
    limpiarTemplate($seccion)
    

    $seccion.style.display = "grid";
    $seccion.style.gridTemplateColumns = `repeat(${cantAristas},1fr)`;
    $seccion.style.gridTemplateRows = `repeat(${cantAristas},1fr)`;

    for(let i = 1; i<=cantAristas*cantAristas; i++){
        let $clone = document.importNode($template, true);
        $fragmento.appendChild($clone);
    }
    $seccion.appendChild($fragmento);

}

const limpiarTemplate = (seccion)=>{
 
    while(seccion.firstChild){
        seccion.firstChild.remove();

    }
    

}

const desactivarVecindad = ()=>{
    $seccion.querySelectorAll(".vecindad-arista").forEach(element => {

        element.checked = false;

    })
    limpiarAristasDelCanvas();

}

const limpiarAristasDelCanvas = ()=>{
    let dibujarCanvas = document.getElementById("canvas").getContext('2d');
    dibujarCanvas.clearRect(0,0,500,500)
    arrVertices.forEach(vertice =>{
        
        dibujarCanvas.beginPath();
        dibujarCanvas.arc(vertice.posX, vertice.posY, radio, 0, pi, false)
        dibujarCanvas.fillStyle = '#006DB2';
        dibujarCanvas.fill();
        dibujarCanvas.stroke();
    })
}





export {dibujarVertice,dibujarVecindad,limpiarCanvas,dibujarVecindad,obtenerIndice,desactivarVecindad}