const url = "http://localhost:3010/obtenerPublicaciones"
const url1 = "http://localhost:3010/buscarPublicacionesUsuario"
const url2 = "http://localhost:3010/borrarPublicacion"

const $template = document.getElementById("publicaciones-borrar").content;
const $seccion = document.querySelector(".ventana-borrar-publicaciones");

const $fragmento = document.createDocumentFragment();




const traerPublicacionesUsuario = async ()=>{
    let usuario = localStorage.getItem("usuario");


    await fetch(url1,{

        method: 'POST',

        body: JSON.stringify({
            "usuario": usuario
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }

    })

} 

/*Obtener todas las publicaciones*/
const obtenerPublicacionesElim = async ()=>{

    await fetch(url,{

        method: 'GET',
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }

    }).then(response => response.json()).then(data => {

        pintarArticulos(data)

    })
}

const eliminarPublicacion = async (elem)=>{
    
    await fetch(url2,{

        method: 'DELETE',

        body: JSON.stringify({
            "id": elem.getAttribute("IdPublicacion")
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }

    })
}



const pintarArticulos = (data)=>{
    limpiarArticulos()
    data.forEach(publicacion => {

        $template.querySelector(".titulo").textContent =  publicacion["titulopublicacion"]
        $template.querySelector(".fecha").textContent =  publicacion["fecha"].slice(0,10);
        $template.querySelector(".btn-borrar").setAttribute("IdPublicacion",publicacion["id"])
        $template.querySelector(".usuario").textContent = publicacion["nombreusuario"];
        
        let $clone = document.importNode($template, true);
        
        $fragmento.appendChild($clone);
    });
    $seccion.appendChild($fragmento);
}

const limpiarArticulos = ()=> {
    while($seccion.firstChild){
        $seccion.firstChild.remove();
    }
}


export {obtenerPublicacionesElim,eliminarPublicacion};