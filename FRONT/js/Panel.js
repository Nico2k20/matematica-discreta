const $d = document;


/*Ocultar Ventana inicio de sesion*/

const ocultarInicioSesion = ()=>{
    
    ocultarMostrarPorPermiso();
    $d.querySelector(".inicio-sesion").style.display = "none";
    $d.querySelector(".log-out").style.display = "block";
}

const inicioDenegado = ()=>{

    cambiarIconoARojo()

    setTimeout(()=>{
        cambiarIconoARojo();

    },5000)

}

const cambiarIconoARojo = ()=>{
    let arrIconos = $d.querySelectorAll(".iconos")
    arrIconos.forEach(element => {
        element.classList.toggle("alerta-permiso")
    });
}




const mostrarInicioSesion = ()=>{
    $d.querySelector(".inicio-sesion").style.display = "flex";
}

const ocultarMostrarPorPermiso = ()=>{
    let permiso = localStorage.getItem("permiso");
    
    if(permiso==="LECTURA"){
        $d.querySelector(".btn-ver-publicaciones").classList.toggle("permiso-escritura")
        $d.querySelector(".btn-crear-publicacion").classList.toggle("ocultar");
        $d.querySelector(".btn-borrar-publicacion").classList.toggle("ocultar");


    }
}

const visualizarCrearPublicaciones =()=>{
    $d.querySelector(".ventana-crear-publicacion").style.display= "inline";
}

const visualizarVerPublicaciones =()=>{
    $d.querySelector(".ventana-ver-publicacion").style.display= "flex";
}

const visualizarBorrarPublicaciones =()=>{
    $d.querySelector(".ventana-borrar-publicaciones").style.display= "flex";
}

const visualizarPublicacion =()=>{
    $d.querySelector(".ventana-visualizar-publicacion").style.display= "flex";
}


const ocultarVentanas =()=>{
    $d.querySelector(".ventana-crear-publicacion").style.display= "none";
    $d.querySelector(".ventana-ver-publicacion").style.display= "none";
    $d.querySelector(".ventana-borrar-publicaciones").style.display= "none";
    $d.querySelector(".ventana-visualizar-publicacion").style.display= "none";
}

const logout = ()=>{

    localStorage.clear()
    $d.querySelector(".log-out").style.display = "none";
    $d.querySelector(".input-usuario").value = "";
    $d.querySelector(".input-contrasena").value = "";
}



export 
{visualizarCrearPublicaciones,
visualizarVerPublicaciones,
ocultarVentanas,
ocultarInicioSesion,
mostrarInicioSesion,
visualizarBorrarPublicaciones,
inicioDenegado,
logout,
visualizarPublicacion};