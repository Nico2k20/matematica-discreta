const publicacionesDAO = require("../DAOS/PublicacionDAO.js")
const UsuariosDAO = require("../DAOS/UsuariosDAO.js")


/*PUBLICACIONES*/
const obtenerTodasPublicaciones = async(req,res)=>{

     await publicacionesDAO.obtenerPublicaciones(req,res)

    

}
const buscarPublicacionesUsuario = async(req,res)=>{

    await publicacionesDAO.buscarPublicacionesUsuario(req,res)
    

}

const buscarPublicacionesPorID = async(req,res)=>{

    await publicacionesDAO.buscarPublicacionPorID(req,res)
    

}



const insertarPublicacion = async(req,res)=>{

    let consulta = await publicacionesDAO.insertarPublicacion(req,res)

    //return consulta;

}

const borrarPublicacion = async (req,res)=>{
    await publicacionesDAO.borrarPublicacion(req,res);
}


//USUARIOS
const identicarUsuario = async(req,res)=>{

    //
    await UsuariosDAO.buscarUsuario(req,res);

}



module.exports = {obtenerTodasPublicaciones,
    buscarPublicacionesUsuario,
    insertarPublicacion,
    identicarUsuario,
    borrarPublicacion,
    buscarPublicacionesPorID}


