drop database if exists APIDISCRETA;
CREATE database APIDISCRETA;
\c apidiscreta


CREATE TABLE usuario(
  id SERIAL,
  usuario varchar(25) PRIMARY KEY,
  passwordUsuario VARCHAR(12),
  permiso varchar(12)
  
);

CREATE TABLE publicacion(
  id SERIAL PRIMARY KEY,
  tituloPublicacion varchar(100),
  introduccionPublicacion VARCHAR(2000),  
  contenidoPublicacion VARCHAR(15000),
  nombreUsuario varchar(20),
  fecha timestamp,
  playground boolean,
  FOREIGN KEY (nombreUsuario) REFERENCES usuario(usuario)
);





INSERT INTO usuario (usuario,passwordUsuario,permiso) VALUES ('nicolas', 'salmon','MAESTRO');
INSERT INTO usuario (usuario,passwordUsuario,permiso) VALUES ('usuario1', 'password1','ESCRITOR');
INSERT INTO usuario (usuario,passwordUsuario,permiso) VALUES ('usuario2', 'password2','LECTURA');



INSERT INTO publicacion (tituloPublicacion,introduccionPublicacion, contenidoPublicacion,nombreUsuario,fecha,playground) VALUES ('Primera publicacion','Introduccion','Contenido de la publicacion','usuario1',now(),'true');

INSERT INTO publicacion (tituloPublicacion,introduccionPublicacion, contenidoPublicacion,nombreUsuario,fecha,playground) VALUES ('Segunda Publicacion','Introduccion','Contenido de la publicacion','usuario1',now(),'false');