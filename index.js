const express = require('express')
const app = express()
const port = 3010
const bodyParser = require('body-parser')
const modelo = require('./modelo/modelo')


app.use(bodyParser.json())

app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

app.use(express.static("FRONT"));



app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


/*ABM PUBLICACIONES*/

app.get("/obtenerPublicaciones", async(req,res)=>{

    (await modelo.obtenerTodasPublicaciones(req,res))

})

app.get("/buscarPublicacionesUsuario", async(req,res)=>{

    await modelo.buscarPublicacionesUsuario(req,res)
})

app.post("/insertarPublicacion", async(req,res)=>{

   

    (await modelo.insertarPublicacion(req,res))

})

app.post("/publicacionPorID",async (req,res)=>{

    await modelo.buscarPublicacionesPorID(req,res)
})


app.delete("/borrarPublicacion",async(req,res)=>{
    await modelo.borrarPublicacion(req,res)
})


/*INICIO DE SESION*/

app.post("/identificacion",async(req,res)=>{

    //
    await modelo.identicarUsuario(req,res);
})





app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })