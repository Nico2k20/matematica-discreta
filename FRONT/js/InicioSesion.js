const $d = document;

const url = "http://localhost:3010/identificacion"




const autentificarUsuario =async ()=>{
    let user = $d.querySelector(".input-usuario").value;
    let pass = $d.querySelector(".input-contrasena").value;

    let usuario =  await buscarUsuario(user,pass)
    
    if(usuario.length>0){
        

        if(usuario[0]["usuario"]===user && usuario[0]["passwordusuario"]===pass){
            localStorage.setItem("permiso", usuario[0]["permiso"])
            localStorage.setItem("usuario", usuario[0]["usuario"])

            return true;

        }
        
    }


    return false;
    /*
        Realizar Algo para remarcarle al usuario que no encontro la cuenta 
    */
}




const buscarUsuario = async (user,pass)=>{

    return await fetch(url,{

        method: 'POST',

        body: JSON.stringify({
            "usuario" : user,
            "password" : pass
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }

    }).then(response => response.json()).then(data => data)
}


export {autentificarUsuario};