const $d = document;
const $cartelPublicacion = $d.querySelector(".cartel-publicacion")


const mostrarCartel = ()=>{
    $cartelPublicacion.style.display = "flex"
}

const ocultarCartel = ()=>{
    $cartelPublicacion.style.display = "none"
}


const mostrarAlertaRealizarPublicacion = ()=>{

    mostrarCartel();
    $cartelPublicacion.querySelector(".contenedor-confirmar-publicacion").style.display = "block";
    $cartelPublicacion.querySelector(".fallo-publicacion").style.display = "none"
}


const mostrarFalloPublicacion = ()=>{
    mostrarCartel();
    $cartelPublicacion.querySelector(".contenedor-confirmar-publicacion").style.display = "none";
    $cartelPublicacion.querySelector(".fallo-publicacion").style.display = "block"
}










export {mostrarCartel,ocultarCartel,mostrarFalloPublicacion,mostrarAlertaRealizarPublicacion};