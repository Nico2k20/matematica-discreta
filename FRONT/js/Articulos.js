const url = "http://localhost:3010/obtenerPublicaciones"
const url1 = "http://localhost:3010/publicacionPorID"
/*Elementos para fragmento*/
const $template = document.getElementById("publicaciones").content;
const $seccion = document.querySelector(".ventana-ver-publicacion");
const $fragmento = document.createDocumentFragment();


/*Elementos crearPublicacion Playground*/
const $d = document;
const btnSI = $d.querySelector(".playground-si");
const btnNO = $d.querySelector(".playground-no")



const obtenerPublicaciones = async ()=>{

    await fetch(url,{

        method: 'GET',
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }

    }).then(response => response.json()).then(data => {

        pintarArticulos(data)

    })
}

const obtenerPublicacion = async (id)=>{

    await fetch(url1,{

        method: 'POST',
        
        body: JSON.stringify({
            "id": id
        }),

        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }

    }).then(response => response.json()).then(data =>{ 
        
        escribirPublicacion(data)
    
    })
}

const escribirPublicacion = (data)=>{
    let datos= data[0];
    $d.querySelector(".titulo-publicacion-visualizar").textContent = datos["titulopublicacion"]

    $d.querySelector(".intro-publicacion").textContent = datos["introduccionpublicacion"]


    $d.querySelector(".contenido-publicacion").textContent = datos["contenidopublicacion"]


    if(!datos["playground"])
        $d.querySelector(".contenedor-playground").style.display= "none";

    else
        $d.querySelector(".contenedor-playground").style.display= "flex";

}


const pintarArticulos = (data)=>{

    limpiarArticulos()
    data.forEach(publicacion => {


        $template.querySelector(".titulo").textContent =  publicacion["titulopublicacion"]

     /*   
        $template.querySelector(".introduccion").textContent =  publicacion["introduccionpublicacion"]
        $template.querySelector(".contenido").textContent =  publicacion["contenidopublicacion"]
     */  
        $template.querySelector(".autor-publicacion").textContent  = "Autor: "+ publicacion["nombreusuario"]
        $template.querySelector(".fecha-publicacion").textContent  = "Fecha de Publicacion: "+publicacion["fecha"].slice(0,10);
        $template.querySelector(".btn-ver-publicacion").setAttribute("nropublicacion",publicacion.id)
        let $clone = document.importNode($template, true);
        
        $fragmento.appendChild($clone);

        
    });
    $seccion.appendChild($fragmento);
}

const limpiarArticulos = ()=>{
    while($seccion.firstChild){
        $seccion.firstChild.remove();
    }
}



const marcarPlayground = (e)=>{
    btnSI.classList.remove("marcar-playground")
    btnNO.classList.remove("marcar-playground")


    e.target.classList.add("marcar-playground");

    


}



export {obtenerPublicaciones,marcarPlayground,obtenerPublicacion}