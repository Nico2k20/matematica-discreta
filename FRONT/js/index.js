import { marcarPlayground, obtenerPublicacion, obtenerPublicaciones } from "./Articulos.js";
import { eliminarPublicacion, obtenerPublicacionesElim } from "./ArticulosEliminacion.js";
import { mostrarAlertaRealizarPublicacion, mostrarCartel, mostrarFalloPublicacion, ocultarCartel } from "./Cartel.js";
import { autentificarUsuario } from "./InicioSesion.js";
import { cambiarPlaygroundNO, cambiarPlaygroundSI, insertarPublicacion, validarCamposPublicacion } from "./insertarPublicaciones.js";
import { inicioDenegado, logout, mostrarInicioSesion, ocultarInicioSesion, ocultarVentanas, visualizarBorrarPublicaciones, visualizarCrearPublicaciones, visualizarPublicacion, visualizarVerPublicaciones } from "./Panel.js";
import { desactivarVecindad, dibujarVertice, limpiarCanvas, obtenerIndice } from "./playground.js";

//Variable document
const $d = document;

//botones
const btnInicioInvitado = ".btn-invitado"
const btnInicioUsuario = ".btn-usuario"
const btnPlaygroundSi = ".playground-si";
const btnPlaygroundNo = ".playground-no";

const mostrarPublicacion = ".btn-crear-publicacion"
const btnPublicar = ".btn-publicar";
const alertaSi = ".btn-publicacion-si"
const alertaNo = ".btn-publicacion-no"
const publicacionesTotales = ".btn-ver-publicaciones"
const borrarPublicacion = ".btn-borrar-publicacion"
const btnBorrarPublicacion = ".btn-borrar"
const iconoSalida = ".icono-logout"
const playground = ".playground-publicacion"
const limpiarPlayground = ".btn-limpiar"
const inputArista = ".vecindad-arista"
const limpiarAristas = ".btn-limpiar-aristas"
const btnVisualizarPublicacion = ".btn-ver-publicacion"
const aceptarAlerta = ".btn-publicacion-aceptar"
let alertaActivada = false;



$d.addEventListener("click", async(e)=>{
   
    //btn-invitado
/*-------------------------------------------INICIO DE SESION-------------------------------------*/
if(e.target.matches(btnInicioInvitado)){
    localStorage.setItem("permiso", "LECTURA")
    ocultarInicioSesion();
    
}

if(e.target.matches(btnInicioUsuario)){
  
    let autentificacion =  await autentificarUsuario()

    autentificacion ?  ocultarInicioSesion() : inicioDenegado();
    
}

if(e.target.matches(iconoSalida)){
    ocultarVentanas();
    logout();
    mostrarInicioSesion();
}


/*-------------------------------------------VENTANA VER PUBLICACIONES-------------------------------------*/
    if(e.target.matches(publicacionesTotales)&&!alertaActivada){
        ocultarVentanas()
        visualizarVerPublicaciones();
        await obtenerPublicaciones();
        
    }

/*-------------------------------------------VENTANA Visualizar PUBLICACION-------------------------------------*/
if(e.target.matches(inputArista)&&!alertaActivada){

   obtenerIndice(e)
}

if(e.target.matches(btnVisualizarPublicacion)&&!alertaActivada){
    ocultarVentanas()
    visualizarPublicacion();
    await obtenerPublicacion(e.target.getAttribute("nropublicacion"))
}



/*-------------------------------------------VENTANA CREAR PUBLICACION--------------------------------------*/
    if(e.target.matches(mostrarPublicacion)&&!alertaActivada){
        ocultarVentanas()
        visualizarCrearPublicaciones();
    }

    if((e.target.matches(btnPlaygroundSi)||e.target.matches(btnPlaygroundNo))&&!alertaActivada){
        marcarPlayground(e);
    }

    if(e.target.matches(btnPlaygroundSi)&&!alertaActivada){
        cambiarPlaygroundSI();
    }
    
    if(e.target.matches(btnPlaygroundNo)&&!alertaActivada){
        cambiarPlaygroundNO();
    }


/*-------------------------------------------VENTANA BORRAR PUBLICACION-------------------------------------*/
    if(e.target.matches(borrarPublicacion)&&!alertaActivada){
        ocultarVentanas()
        visualizarBorrarPublicaciones();
        obtenerPublicacionesElim();
       
    }



/*-------------------------------------------CARTEL ALERTA--------------------------------------------------*/
    if(e.target.matches(btnPublicar)&&!alertaActivada){
        alertaActivada=true;
        validarCamposPublicacion() ? mostrarAlertaRealizarPublicacion() : mostrarFalloPublicacion(); 
    }

    if(e.target.matches(alertaSi)){
        await insertarPublicacion();
        ocultarCartel();
        alertaActivada =false
        ocultarVentanas()
    }

    if(e.target.matches(alertaNo)){
        ocultarCartel();
        alertaActivada=false
    }

    if(e.target.matches(aceptarAlerta)){
        ocultarCartel();
        alertaActivada=false
    }

/*ACCIONES*/

/*------------------------------------------------------PLAYGROUND CANVAS------------------------------------*/

if(e.target.matches(playground)&&!alertaActivada){
    dibujarVertice(e);
}

if(e.target.matches(limpiarPlayground)&&!alertaActivada){
    limpiarCanvas();
}

if(e.target.matches(limpiarAristas)&&!alertaActivada){
    desactivarVecindad();
}



//BORRAR PUBLICACION

if(e.target.matches(btnBorrarPublicacion)&&!alertaActivada){
    console.log(e.target);
    await eliminarPublicacion(e.target);
    await obtenerPublicacionesElim()

}




})




