const url = "http://localhost:3010/insertarPublicacion"
const $d = document;
const titulo =  $d.querySelector(".input-titulo-publicacion")
const introduccion =  $d.querySelector(".input-introduccion-publicacion")
const contenido =  $d.querySelector(".input-contenido-publicacion")
let playgroundSI = false

const insertarPublicacion = async()=>{




    return await fetch(url,{

        method: 'POST',

        body: JSON.stringify({
            "titulo": titulo.value,
            "introduccion": introduccion.value,
            "contenido" : contenido.value,
            "usuario" : localStorage.getItem("usuario"),
            "playground" : playgroundSI
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }

    })


 
}

const validarCamposPublicacion = ()=>{


    let caracteresTitulo = titulo.value.length>=3
    let caracteresIntro = introduccion.value.length >= 10;
    let caracteresContenido = contenido.value.length >=20;

    return caracteresTitulo && caracteresIntro & caracteresContenido;

}



const cambiarPlaygroundSI = ()=>{
    playgroundSI = true
}


const cambiarPlaygroundNO = ()=>{
    playgroundSI = false
}


export {insertarPublicacion,cambiarPlaygroundSI,cambiarPlaygroundNO,validarCamposPublicacion}