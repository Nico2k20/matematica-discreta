const {conexion} = require("../conexionBD/ConexionBD.js")

const readPublicaciones = "SELECT * FROM publicacion";

const readPublicacionesPorID= "SELECT * FROM publicacion WHERE id = $1"

const insertPublicacion = "INSERT INTO publicacion (tituloPublicacion,introduccionPublicacion, contenidoPublicacion,nombreUsuario,fecha,playground) VALUES ($1,$2,$3,$4,now(),$5);"

const deletePublicacion = "delete from publicacion where id = $1";


const obtenerPublicaciones = async (req,res)=>{
    //console.log(conexion)
    let conexionABD = conexion()
    conexionABD.connect()

    let resultado;

    await conexionABD.query(readPublicaciones).then(response =>{ 
        resultado = (response.rows)
        conexionABD.end()
    }).catch(err =>{
        console.log(err)
        conexionABD.end();
    })


    res.status(200).send(resultado)

}


const buscarPublicacionPorID  =  async (req,res)=>{
    let conexionABD = conexion()
    conexionABD.connect()
    const {id} = req.body

    let resultado;

    await conexionABD.query(readPublicacionesPorID,[id]).then(response =>{ 
        resultado = (response.rows)
        conexionABD.end()
    }).catch(err =>{
        console.log(err)
        conexionABD.end();
    })


    res.status(200).send(resultado)


}



const buscarPublicacionesUsuario=  async (req,res)=>{
    let conexionABD = conexion()
    conexionABD.connect()

    let resultado;
    let codigo = 200

    await conexionABD.query(readPublicacionesPorUsuario).then(response =>{ 
        resultado = (response.rows)
        conexionABD.end()
    }).catch(err =>{
        codigo = 500;
        console.log(err)
        conexionABD.end();
    })


    res.status(codigo).send(resultado)
}


const insertarPublicacion = async (req,res)=>{
    //console.log(conexion)
    let conexionABD = conexion()
    conexionABD.connect()

   // console.log(req.body)

    const {titulo,introduccion,contenido,usuario,playground} = req.body


    let status;
    let resultado;

    await conexionABD.query(insertPublicacion,[titulo,introduccion,contenido,usuario,playground]).then(response =>{ 
        resultado = "INSERT EXITOSO"
        status = 200
        conexionABD.end()
    }).catch(err =>{

        console.log(err)
        resultado = "INSERT FALLIDO"
        status = 500
        conexionABD.end();
    })


    res.status(status).send(resultado);


}


const borrarPublicacion = async (req,res)=>{
    let conexionABD = conexion()
    conexionABD.connect()

    const {id} = req.body


    let status;
    let resultado;


    await conexionABD.query(deletePublicacion,[id]).then(response =>{ 
        resultado = "Publicacion Eliminada"
        status = 200
        conexionABD.end()
    }).catch(err =>{

        console.log(err)
        resultado = "Error en la eliminacion"
        status = 500
        conexionABD.end();
    })


    res.status(status).send(resultado);
}



module.exports = {obtenerPublicaciones,borrarPublicacion,buscarPublicacionesUsuario,insertarPublicacion,buscarPublicacionPorID}
