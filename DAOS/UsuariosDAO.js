const {conexion} = require("../conexionBD/ConexionBD.js")


const read = "SELECT * FROM usuario;"

const identificar = "SELECT * FROM usuario WHERE usuario = $1 AND passwordUsuario = $2;"




const buscarUsuario = async(req,res)=>{

    const {usuario,password} = req.body;
    let conexionABD = conexion();
    conexionABD.connect();

    let resultadoQuery;
    let cod;

    await conexionABD.query(identificar,[usuario,password]).then(response => {

        resultadoQuery = response.rows;
        cod = 201
        conexionABD.end()
    }).catch(err => {
       
        console.log("Ha ocurrido un error en la identificion")
        console.log("<-------------------------------------->")
        console.log(err)
        cod = 500
        conexionABD.end()
    })

    res.status(cod).send(resultadoQuery)
}



module.exports = {buscarUsuario}



